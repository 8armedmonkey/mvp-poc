package com.eightam.mvp.poc.coordination;

import com.eightam.mvp.poc.domain.User;
import com.eightam.mvp.poc.domain.UserAppService;
import com.eightam.mvp.poc.navigation.SetupNavigator;

public class FreeSetupCoordinator extends SetupCoordinator {

    public FreeSetupCoordinator(UserAppService userAppService,
                                SetupNavigator setupNavigator) {
        super(userAppService, setupNavigator);
    }

    @Override
    protected void proceedWithUser(User user) {
        setupNavigator.enterFullscreenAdScreen();
    }

}

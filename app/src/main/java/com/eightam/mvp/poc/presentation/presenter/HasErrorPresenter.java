package com.eightam.mvp.poc.presentation.presenter;

import android.os.Bundle;

public interface HasErrorPresenter {

    void presentError(Throwable t, Bundle args);

}

package com.eightam.mvp.poc.navigation;

import android.os.Bundle;

public interface Navigator {

    void enterErrorScreen(Throwable t, Bundle args);

}

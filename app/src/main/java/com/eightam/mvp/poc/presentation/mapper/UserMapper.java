package com.eightam.mvp.poc.presentation.mapper;

import com.eightam.mvp.poc.domain.User;
import com.eightam.mvp.poc.presentation.model.UserPresentation;

public class UserMapper {

    public static UserPresentation map(User user) {
        UserPresentation userPresentation = new UserPresentation();
        userPresentation.setId(user.getId());
        userPresentation.setFirstName(user.getFirstName());
        userPresentation.setLastName(user.getLastName());

        return userPresentation;
    }

}

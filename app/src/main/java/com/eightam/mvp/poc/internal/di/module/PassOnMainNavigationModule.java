package com.eightam.mvp.poc.internal.di.module;

import com.eightam.mvp.poc.navigation.MainNavigator;

import dagger.Module;
import dagger.Provides;

@Module
public class PassOnMainNavigationModule {

    private MainNavigator mainNavigator;

    public PassOnMainNavigationModule(MainNavigator mainNavigator) {
        this.mainNavigator = mainNavigator;
    }

    @Provides
    public MainNavigator provideNavigator() {
        return mainNavigator;
    }

}

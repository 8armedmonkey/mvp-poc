package com.eightam.mvp.poc.internal.di.component.base;

import com.eightam.mvp.poc.navigation.SetupNavigator;

public interface BaseSetupNavigationComponent {

    SetupNavigator setupNavigator();

}

package com.eightam.mvp.poc.user;

import com.eightam.mvp.poc.domain.User;
import com.eightam.mvp.poc.domain.UserService;

import java.util.UUID;

public class UserServiceImpl implements UserService {

    @Override
    public User register(String firstName, String lastName) {
        return new User(UUID.randomUUID().toString(), firstName, lastName);
    }

}

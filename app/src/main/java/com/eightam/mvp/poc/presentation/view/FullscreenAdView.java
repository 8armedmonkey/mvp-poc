package com.eightam.mvp.poc.presentation.view;

public interface FullscreenAdView {

    void setListener(Listener listener);

    interface Listener {

        void onFullscreenAdClosed();

    }

}

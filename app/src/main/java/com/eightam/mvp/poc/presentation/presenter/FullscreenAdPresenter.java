package com.eightam.mvp.poc.presentation.presenter;

import com.eightam.mvp.poc.presentation.view.FullscreenAdView;

public class FullscreenAdPresenter
        extends BasePresenter<FullscreenAdView, FullscreenAdPresenter.Delegate>
        implements FullscreenAdView.Listener {

    @Override
    public void bindView(FullscreenAdView v) {
        super.bindView(v);
        v.setListener(FullscreenAdPresenter.this);
    }

    @Override
    public void onFullscreenAdClosed() {
        if (delegate != null) {
            delegate.onFullscreenAdClosed();
        }
    }

    public interface Delegate {

        void onFullscreenAdClosed();

    }

}

package com.eightam.mvp.poc.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.eightam.mvp.poc.R;
import com.eightam.mvp.poc.coordination.SetupCoordinator;
import com.eightam.mvp.poc.internal.di.component.DaggerSetupFragmentComponent;
import com.eightam.mvp.poc.internal.di.component.SetupActivityComponent;
import com.eightam.mvp.poc.internal.di.module.SetupPresentationModule;
import com.eightam.mvp.poc.presentation.presenter.WelcomePresenter;
import com.eightam.mvp.poc.presentation.view.WelcomeView;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class WelcomeFragment extends BaseFragment implements WelcomeView {

    @Inject
    protected SetupCoordinator setupCoordinator;

    @Inject
    protected WelcomePresenter presenter;

    private Listener listener;

    public static WelcomeFragment newInstance() {
        return new WelcomeFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.view_welcome, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(WelcomeFragment.this, view);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initializeDi();

        presenter.onCreate(savedInstanceState);
        presenter.bindContext(getActivity());
        presenter.bindView(WelcomeFragment.this);
        presenter.bindDelegate(setupCoordinator);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        if (presenter != null) {
            presenter.onSaveInstanceState(outState);
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if (presenter != null) {
            presenter.onResume();
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        if (presenter != null) {
            presenter.onPause();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if (presenter != null) {
            presenter.onDestroy();
        }
    }

    @Override
    public void setListener(Listener listener) {
        this.listener = listener;
    }

    @OnClick(R.id.button_start_registration)
    protected void startRegistrationFromUi() {
        notifyListenerOnNavigateAwayFromWelcome();
    }

    private void initializeDi() {
        SetupActivityComponent setupActivityComponent =
                getActivityComponent(SetupActivityComponent.class);

        DaggerSetupFragmentComponent.builder()
                .setupActivityComponent(setupActivityComponent)
                .setupPresentationModule(new SetupPresentationModule())
                .build()
                .inject(WelcomeFragment.this);
    }

    private void notifyListenerOnNavigateAwayFromWelcome() {
        if (listener != null) {
            listener.onNavigateAwayFromWelcome();
        }
    }

}

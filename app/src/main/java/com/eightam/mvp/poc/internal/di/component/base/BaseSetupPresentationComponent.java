package com.eightam.mvp.poc.internal.di.component.base;

import com.eightam.mvp.poc.presentation.presenter.RegistrationPresenter;
import com.eightam.mvp.poc.presentation.presenter.WelcomePresenter;

public interface BaseSetupPresentationComponent {

    WelcomePresenter welcomePresenter();

    RegistrationPresenter registrationPresenter();

}

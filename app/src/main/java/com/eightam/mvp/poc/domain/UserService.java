package com.eightam.mvp.poc.domain;

public interface UserService {

    User register(String firstName, String lastName);

}

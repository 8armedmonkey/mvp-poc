package com.eightam.mvp.poc.navigation;

public interface FullscreenAdNavigator extends Navigator {

    void enterFullscreenAdScreen();

    void enterHomeScreen();

}

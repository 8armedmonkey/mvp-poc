package com.eightam.mvp.poc.coordination;

import android.content.Context;
import android.os.Bundle;

public abstract class BaseCoordinator implements Coordinator {

    protected Context context;

    @Override
    public void onCreate(Bundle savedInstanceState) {
    }

    @Override
    public void onDestroy() {
    }

    @Override
    public void bindContext(Context context) {
        this.context = context;
    }

    @Override
    public void unbindContext() {
        this.context = null;
    }
}

package com.eightam.mvp.poc.domain;

public class NoRegisteredUserException extends Exception {

    public NoRegisteredUserException() {
        super();
    }

    public NoRegisteredUserException(String detailMessage) {
        super(detailMessage);
    }

    public NoRegisteredUserException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public NoRegisteredUserException(Throwable throwable) {
        super(throwable);
    }

}

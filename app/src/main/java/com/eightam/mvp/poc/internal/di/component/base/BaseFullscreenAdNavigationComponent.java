package com.eightam.mvp.poc.internal.di.component.base;

import com.eightam.mvp.poc.navigation.FullscreenAdNavigator;

public interface BaseFullscreenAdNavigationComponent {

    FullscreenAdNavigator fullscreenAdNavigator();

}

package com.eightam.mvp.poc.user;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import com.eightam.mvp.poc.domain.User;
import com.eightam.mvp.poc.domain.UserRepository;

public class UserRepositoryImpl implements UserRepository {

    private static final String PREFS_NAME = "userRepository";
    private static final String KEY_ID = "id";
    private static final String KEY_FIRST_NAME = "firstName";
    private static final String KEY_LAST_NAME = "lastName";

    private SharedPreferences sharedPreferences;

    public UserRepositoryImpl(Context context) {
        sharedPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
    }

    @Override
    public User getUser() {
        String id = sharedPreferences.getString(KEY_ID, "");
        String firstName = sharedPreferences.getString(KEY_FIRST_NAME, "");
        String lastName = sharedPreferences.getString(KEY_LAST_NAME, "");

        if (!TextUtils.isEmpty(id)) {
            return new User(id, firstName, lastName);
        } else {
            return null;
        }
    }

    @Override
    public void setUser(User user) {
        sharedPreferences.edit()
                .putString(KEY_ID, user.getId())
                .putString(KEY_FIRST_NAME, user.getFirstName())
                .putString(KEY_LAST_NAME, user.getLastName())
                .apply();
    }

}

package com.eightam.mvp.poc.internal.di.module;

import com.eightam.mvp.poc.presentation.presenter.RegistrationPresenter;
import com.eightam.mvp.poc.presentation.presenter.WelcomePresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class SetupPresentationModule {

    private WelcomePresenter welcomePresenter;
    private RegistrationPresenter registrationPresenter;

    @Provides
    public WelcomePresenter provideWelcomePresenter() {
        if (welcomePresenter == null) {
            welcomePresenter = new WelcomePresenter();
        }
        return welcomePresenter;
    }

    @Provides
    public RegistrationPresenter provideRegistrationPresenter() {
        if (registrationPresenter == null) {
            registrationPresenter = new RegistrationPresenter();
        }
        return registrationPresenter;
    }

}

package com.eightam.mvp.poc.presentation.presenter;

import android.content.Context;
import android.os.Bundle;

import rx.subscriptions.CompositeSubscription;

public abstract class BasePresenter<View, Delegate> implements Presenter<View, Delegate> {

    protected CompositeSubscription compositeSubscription;
    protected Context context;
    protected View view;
    protected Delegate delegate;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        initializeCompositeSubscription();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
    }

    @Override
    public void onResume() {
    }

    @Override
    public void onPause() {
    }

    @Override
    public void onDestroy() {
        finalizeCompositeSubscription();
        unbindContext();
        unbindView();
        unbindDelegate();
    }

    @Override
    public void bindContext(Context context) {
        this.context = context;
    }

    @Override
    public void unbindContext() {
        this.context = null;
    }

    @Override
    public void bindView(View v) {
        this.view = v;
    }

    @Override
    public void unbindView() {
        this.view = null;
    }

    @Override
    public void bindDelegate(Delegate d) {
        this.delegate = d;
    }

    @Override
    public void unbindDelegate() {
        this.delegate = null;
    }

    protected void initializeCompositeSubscription() {
        compositeSubscription = new CompositeSubscription();
    }

    protected void finalizeCompositeSubscription() {
        compositeSubscription.unsubscribe();
    }

}

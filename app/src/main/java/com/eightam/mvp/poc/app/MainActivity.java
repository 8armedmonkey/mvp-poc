package com.eightam.mvp.poc.app;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import com.eightam.mvp.poc.R;
import com.eightam.mvp.poc.coordination.MainCoordinator;
import com.eightam.mvp.poc.fragment.HomeFragment;
import com.eightam.mvp.poc.internal.di.HasComponent;
import com.eightam.mvp.poc.internal.di.component.DaggerMainActivityComponent;
import com.eightam.mvp.poc.internal.di.component.MainActivityComponent;
import com.eightam.mvp.poc.internal.di.module.MainCoordinationModule;
import com.eightam.mvp.poc.internal.di.module.PassOnMainNavigationModule;
import com.eightam.mvp.poc.internal.di.module.UserAppServiceModule;
import com.eightam.mvp.poc.navigation.MainNavigator;
import com.eightam.mvp.poc.presentation.presenter.HasErrorPresenter;

import javax.inject.Inject;

public class MainActivity
        extends AppCompatActivity
        implements
        MainNavigator,
        HasComponent<MainActivityComponent> {

    private static final String FRAGMENT_HOME = "home";

    protected MainActivityComponent mainActivityComponent;

    @Inject
    protected MainCoordinator mainCoordinator;

    public static Intent getStartIntent(Context context) {
        return new Intent(context, MainActivity.class);
    }

    @Override
    public void enterHomeScreen() {
        HomeFragment homeFragment =
                (HomeFragment) getSupportFragmentManager().findFragmentByTag(FRAGMENT_HOME);

        if (homeFragment == null) {
            homeFragment = HomeFragment.newInstance();

            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.content, homeFragment, FRAGMENT_HOME)
                    .commit();
        }
    }

    @Override
    public void enterErrorScreen(Throwable t, Bundle args) {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.content);

        if (fragment != null && fragment instanceof HasErrorPresenter) {
            ((HasErrorPresenter) fragment).presentError(t, args);
        }
    }

    @Override
    public MainActivityComponent getComponent() {
        return mainActivityComponent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initializeDi();
        mainCoordinator.onCreate(savedInstanceState);
    }

    private void initializeDi() {
        mainActivityComponent = DaggerMainActivityComponent.builder()
                .userAppServiceModule(new UserAppServiceModule(MainActivity.this))
                .mainCoordinationModule(new MainCoordinationModule())
                .passOnMainNavigationModule(new PassOnMainNavigationModule(MainActivity.this))
                .build();

        mainActivityComponent.inject(MainActivity.this);
    }

}

package com.eightam.mvp.poc.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.eightam.mvp.poc.R;
import com.eightam.mvp.poc.internal.di.component.DaggerMainFragmentComponent;
import com.eightam.mvp.poc.internal.di.component.MainActivityComponent;
import com.eightam.mvp.poc.internal.di.module.MainPresentationModule;
import com.eightam.mvp.poc.presentation.presenter.HomePresenter;
import com.eightam.mvp.poc.presentation.view.HomeView;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

public class HomeFragment extends BaseFragment implements HomeView {

    @Inject
    protected HomePresenter presenter;

    @Bind(R.id.text_message)
    protected TextView textMessage;

    public static HomeFragment newInstance() {
        return new HomeFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.view_home, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(HomeFragment.this, view);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initializeDi();

        presenter.onCreate(savedInstanceState);
        presenter.bindContext(getActivity());
        presenter.bindView(HomeFragment.this);
        presenter.showMessageToCurrentUser();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        if (presenter != null) {
            presenter.onSaveInstanceState(outState);
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if (presenter != null) {
            presenter.onResume();
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        if (presenter != null) {
            presenter.onPause();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if (presenter != null) {
            presenter.onDestroy();
        }
    }

    @Override
    public void showUserMessage(String message) {
        textMessage.setText(message);
    }

    private void initializeDi() {
        MainActivityComponent mainActivityComponent =
                getActivityComponent(MainActivityComponent.class);

        DaggerMainFragmentComponent.builder()
                .mainActivityComponent(mainActivityComponent)
                .mainPresentationModule(new MainPresentationModule())
                .build()
                .inject(HomeFragment.this);
    }

}

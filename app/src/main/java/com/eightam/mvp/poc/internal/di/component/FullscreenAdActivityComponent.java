package com.eightam.mvp.poc.internal.di.component;

import com.eightam.mvp.poc.app.FullscreenAdActivity;
import com.eightam.mvp.poc.internal.di.component.base.BaseFullscreenAdCoordinationComponent;
import com.eightam.mvp.poc.internal.di.component.base.BaseFullscreenAdNavigationComponent;
import com.eightam.mvp.poc.internal.di.module.FullscreenAdCoordinationModule;
import com.eightam.mvp.poc.internal.di.module.PassOnFullscreenAdNavigationModule;

import dagger.Component;

@Component(
        modules = {
                FullscreenAdCoordinationModule.class,
                PassOnFullscreenAdNavigationModule.class
        }
)
public interface FullscreenAdActivityComponent
        extends
        BaseFullscreenAdCoordinationComponent,
        BaseFullscreenAdNavigationComponent {

    void inject(FullscreenAdActivity fullscreenAdActivity);

}

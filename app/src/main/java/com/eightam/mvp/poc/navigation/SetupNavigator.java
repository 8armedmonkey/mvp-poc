package com.eightam.mvp.poc.navigation;

public interface SetupNavigator extends Navigator {

    void enterWelcomeScreen();

    void enterRegistrationScreen();

    void enterFullscreenAdScreen();

    void enterMainScreen();

}

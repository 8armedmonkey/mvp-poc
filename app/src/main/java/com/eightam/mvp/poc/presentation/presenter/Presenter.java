package com.eightam.mvp.poc.presentation.presenter;

import android.content.Context;
import android.os.Bundle;

public interface Presenter<View, Delegate> {

    void onCreate(Bundle savedInstanceState);

    void onSaveInstanceState(Bundle outState);

    void onResume();

    void onPause();

    void onDestroy();

    void bindContext(Context context);

    void unbindContext();

    void bindView(View v);

    void unbindView();

    void bindDelegate(Delegate d);

    void unbindDelegate();

}

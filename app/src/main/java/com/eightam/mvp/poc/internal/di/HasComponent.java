package com.eightam.mvp.poc.internal.di;

public interface HasComponent<T> {

    T getComponent();

}

package com.eightam.mvp.poc.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.eightam.mvp.poc.R;
import com.eightam.mvp.poc.coordination.SetupCoordinator;
import com.eightam.mvp.poc.internal.di.component.DaggerSetupFragmentComponent;
import com.eightam.mvp.poc.internal.di.component.SetupActivityComponent;
import com.eightam.mvp.poc.internal.di.module.SetupPresentationModule;
import com.eightam.mvp.poc.presentation.presenter.HasErrorPresenter;
import com.eightam.mvp.poc.presentation.presenter.RegistrationPresenter;
import com.eightam.mvp.poc.presentation.view.RegistrationView;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RegistrationFragment extends BaseFragment implements RegistrationView, HasErrorPresenter {

    @Inject
    protected SetupCoordinator setupCoordinator;

    @Inject
    protected RegistrationPresenter presenter;

    @Bind(R.id.edit_first_name)
    protected EditText editFirstName;

    @Bind(R.id.edit_last_name)
    protected EditText editLastName;

    private Listener listener;

    public static RegistrationFragment newInstance() {
        return new RegistrationFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.view_registration, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(RegistrationFragment.this, view);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initializeDi();

        presenter.onCreate(savedInstanceState);
        presenter.bindContext(getActivity());
        presenter.bindView(RegistrationFragment.this);
        presenter.bindDelegate(setupCoordinator);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        if (presenter != null) {
            presenter.onSaveInstanceState(outState);
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if (presenter != null) {
            presenter.onResume();
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        if (presenter != null) {
            presenter.onPause();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if (presenter != null) {
            presenter.onDestroy();
        }
    }

    @Override
    public void setListener(Listener listener) {
        this.listener = listener;
    }

    @Override
    public void presentError(Throwable t, Bundle args) {
        View view = getView();

        if (view != null) {
            Snackbar.make(view, t.getMessage(), Snackbar.LENGTH_SHORT).show();
        }
    }

    @OnClick(R.id.button_register)
    protected void registerFromUi() {
        String firstName = editFirstName.getText().toString();
        String lastName = editLastName.getText().toString();

        notifyListenerOnUserDataSubmitted(firstName, lastName);
    }

    private void initializeDi() {
        SetupActivityComponent setupActivityComponent =
                getActivityComponent(SetupActivityComponent.class);

        DaggerSetupFragmentComponent.builder()
                .setupActivityComponent(setupActivityComponent)
                .setupPresentationModule(new SetupPresentationModule())
                .build()
                .inject(RegistrationFragment.this);
    }

    private void notifyListenerOnUserDataSubmitted(String firstName, String lastName) {
        if (listener != null) {
            listener.onUserDataSubmitted(firstName, lastName);
        }
    }

}

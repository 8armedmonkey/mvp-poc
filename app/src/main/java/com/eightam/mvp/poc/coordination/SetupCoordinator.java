package com.eightam.mvp.poc.coordination;

import android.os.Bundle;

import com.eightam.mvp.poc.domain.User;
import com.eightam.mvp.poc.domain.UserAppService;
import com.eightam.mvp.poc.navigation.SetupNavigator;
import com.eightam.mvp.poc.presentation.presenter.RegistrationPresenter;
import com.eightam.mvp.poc.presentation.presenter.WelcomePresenter;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public abstract class SetupCoordinator
        extends BaseCoordinator
        implements
        WelcomePresenter.Delegate,
        RegistrationPresenter.Delegate {

    protected UserAppService userAppService;
    protected SetupNavigator setupNavigator;

    public SetupCoordinator(UserAppService userAppService,
                            SetupNavigator setupNavigator) {
        this.userAppService = userAppService;
        this.setupNavigator = setupNavigator;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        checkSetupStatusAndProceed();
    }

    @Override
    public void onNavigateAwayFromWelcome() {
        setupNavigator.enterRegistrationScreen();
    }

    @Override
    public void onUserDataSubmitted(String firstName, String lastName) {
        userAppService.register(firstName, lastName)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::proceedWithUser, this::proceedWithRegistrationError);
    }

    protected void checkSetupStatusAndProceed() {
        userAppService.getCurrentUser()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::proceedWithUser, this::proceedWithoutUser);
    }

    protected void proceedWithoutUser(Throwable t) {
        setupNavigator.enterWelcomeScreen();
    }

    protected void proceedWithRegistrationError(Throwable t) {
        setupNavigator.enterErrorScreen(t, Bundle.EMPTY);
    }

    protected abstract void proceedWithUser(User user);

}

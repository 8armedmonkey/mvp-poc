package com.eightam.mvp.poc.internal.di.component.base;

import com.eightam.mvp.poc.coordination.SetupCoordinator;

public interface BaseSetupCoordinationComponent {

    SetupCoordinator setupCoordinator();

}

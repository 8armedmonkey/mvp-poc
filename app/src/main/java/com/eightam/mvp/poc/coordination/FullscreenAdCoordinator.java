package com.eightam.mvp.poc.coordination;

import android.os.Bundle;

import com.eightam.mvp.poc.navigation.FullscreenAdNavigator;
import com.eightam.mvp.poc.presentation.presenter.FullscreenAdPresenter;

public class FullscreenAdCoordinator
        extends BaseCoordinator
        implements FullscreenAdPresenter.Delegate {

    private FullscreenAdNavigator fullscreenAdNavigator;

    public FullscreenAdCoordinator(FullscreenAdNavigator fullscreenAdNavigator) {
        this.fullscreenAdNavigator = fullscreenAdNavigator;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null) {
            fullscreenAdNavigator.enterFullscreenAdScreen();
        }
    }

    @Override
    public void onFullscreenAdClosed() {
        fullscreenAdNavigator.enterHomeScreen();
    }

}

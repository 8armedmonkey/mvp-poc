package com.eightam.mvp.poc.internal.di.component;

import com.eightam.mvp.poc.fragment.HomeFragment;
import com.eightam.mvp.poc.internal.di.component.base.BaseMainPresentationComponent;
import com.eightam.mvp.poc.internal.di.module.MainPresentationModule;

import dagger.Component;

@Component(
        dependencies = {
                MainActivityComponent.class
        },
        modules = {
                MainPresentationModule.class
        }
)
public interface MainFragmentComponent
        extends
        MainActivityComponent,
        BaseMainPresentationComponent {

    void inject(HomeFragment homeFragment);

}

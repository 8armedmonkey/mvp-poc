package com.eightam.mvp.poc.internal.di.module;

import com.eightam.mvp.poc.domain.UserAppService;
import com.eightam.mvp.poc.presentation.presenter.HomePresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class MainPresentationModule {

    private HomePresenter homePresenter;

    @Provides
    public HomePresenter provideHomePresenter(UserAppService userAppService) {
        if (homePresenter == null) {
            homePresenter = new HomePresenter(userAppService);
        }
        return homePresenter;
    }

}

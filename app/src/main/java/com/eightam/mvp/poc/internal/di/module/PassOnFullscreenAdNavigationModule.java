package com.eightam.mvp.poc.internal.di.module;

import com.eightam.mvp.poc.navigation.FullscreenAdNavigator;

import dagger.Module;
import dagger.Provides;

@Module
public class PassOnFullscreenAdNavigationModule {

    private FullscreenAdNavigator fullscreenAdNavigator;

    public PassOnFullscreenAdNavigationModule(FullscreenAdNavigator fullscreenAdNavigator) {
        this.fullscreenAdNavigator = fullscreenAdNavigator;
    }

    @Provides
    public FullscreenAdNavigator provideNavigator() {
        return fullscreenAdNavigator;
    }

}

package com.eightam.mvp.poc.internal.di.component;

import com.eightam.mvp.poc.fragment.FullscreenAdFragment;
import com.eightam.mvp.poc.internal.di.component.base.BaseFullscreenAdPresentationComponent;
import com.eightam.mvp.poc.internal.di.module.FullscreenAdPresentationModule;

import dagger.Component;

@Component(
        dependencies = {
                FullscreenAdActivityComponent.class
        },
        modules = {
                FullscreenAdPresentationModule.class
        }
)
public interface FullscreenAdFragmentComponent
        extends
        FullscreenAdActivityComponent,
        BaseFullscreenAdPresentationComponent {

    void inject(FullscreenAdFragment fullscreenAdFragment);
}

package com.eightam.mvp.poc.domain;

public interface UserRepository {

    User getUser();

    void setUser(User user);

}

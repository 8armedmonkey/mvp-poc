package com.eightam.mvp.poc.presentation.presenter;

import com.eightam.mvp.poc.presentation.view.RegistrationView;

public class RegistrationPresenter
        extends BasePresenter<RegistrationView, RegistrationPresenter.Delegate>
        implements RegistrationView.Listener {

    @Override
    public void bindView(RegistrationView v) {
        super.bindView(v);
        v.setListener(RegistrationPresenter.this);
    }

    @Override
    public void onUserDataSubmitted(String firstName, String lastName) {
        if (delegate != null) {
            delegate.onUserDataSubmitted(firstName, lastName);
        }
    }

    public interface Delegate {

        void onUserDataSubmitted(String firstName, String lastName);

    }

}

package com.eightam.mvp.poc.internal.di.component.base;

import com.eightam.mvp.poc.presentation.presenter.HomePresenter;

public interface BaseMainPresentationComponent {

    HomePresenter homePresenter();

}

package com.eightam.mvp.poc.navigation;

public interface MainNavigator extends Navigator {

    void enterHomeScreen();

}

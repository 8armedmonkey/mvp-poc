package com.eightam.mvp.poc.internal.di.module;

import com.eightam.mvp.poc.coordination.FullscreenAdCoordinator;
import com.eightam.mvp.poc.navigation.FullscreenAdNavigator;

import dagger.Module;
import dagger.Provides;

@Module
public class FullscreenAdCoordinationModule {

    private FullscreenAdCoordinator fullscreenAdCoordinator;

    @Provides
    public FullscreenAdCoordinator provideFullscreenAdCoordinator(FullscreenAdNavigator fullscreenAdNavigator) {
        if (fullscreenAdCoordinator == null) {
            fullscreenAdCoordinator = new FullscreenAdCoordinator(fullscreenAdNavigator);
        }
        return fullscreenAdCoordinator;
    }

}

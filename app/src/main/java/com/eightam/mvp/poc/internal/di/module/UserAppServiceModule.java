package com.eightam.mvp.poc.internal.di.module;

import android.content.Context;

import com.eightam.mvp.poc.domain.UserAppService;
import com.eightam.mvp.poc.domain.UserRepository;
import com.eightam.mvp.poc.domain.UserService;
import com.eightam.mvp.poc.user.UserRepositoryImpl;
import com.eightam.mvp.poc.user.UserServiceImpl;

import dagger.Module;
import dagger.Provides;

@Module
public class UserAppServiceModule {

    private Context context;
    private UserService userService;
    private UserRepository userRepository;
    private UserAppService userAppService;

    public UserAppServiceModule(Context context) {
        this.context = context;
    }

    @Provides
    public UserService provideUserService() {
        if (userService == null) {
            userService = new UserServiceImpl();
        }
        return userService;
    }

    @Provides
    public UserRepository provideUserRepository() {
        if (userRepository == null) {
            userRepository = new UserRepositoryImpl(context);
        }
        return userRepository;
    }

    @Provides
    public UserAppService provideUserAppService(UserService userService,
                                                UserRepository userRepository) {
        if (userAppService == null) {
            userAppService = new UserAppService(userService, userRepository);
        }
        return userAppService;
    }

}

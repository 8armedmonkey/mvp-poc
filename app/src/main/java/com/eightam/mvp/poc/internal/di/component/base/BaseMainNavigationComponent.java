package com.eightam.mvp.poc.internal.di.component.base;

import com.eightam.mvp.poc.navigation.MainNavigator;

public interface BaseMainNavigationComponent {

    MainNavigator mainNavigator();

}

package com.eightam.mvp.poc.coordination;

import android.os.Bundle;

import com.eightam.mvp.poc.navigation.MainNavigator;

public class MainCoordinator extends BaseCoordinator {

    private MainNavigator mainNavigator;

    public MainCoordinator(MainNavigator mainNavigator) {
        this.mainNavigator = mainNavigator;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null) {
            mainNavigator.enterHomeScreen();
        }
    }

}

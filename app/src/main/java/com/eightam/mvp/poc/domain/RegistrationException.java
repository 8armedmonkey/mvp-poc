package com.eightam.mvp.poc.domain;

public class RegistrationException extends Exception {

    public RegistrationException() {
    }

    public RegistrationException(String detailMessage) {
        super(detailMessage);
    }

    public RegistrationException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public RegistrationException(Throwable throwable) {
        super(throwable);
    }

}

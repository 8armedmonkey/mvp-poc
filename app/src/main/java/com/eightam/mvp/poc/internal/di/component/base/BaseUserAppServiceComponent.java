package com.eightam.mvp.poc.internal.di.component.base;

import com.eightam.mvp.poc.domain.UserAppService;
import com.eightam.mvp.poc.domain.UserRepository;
import com.eightam.mvp.poc.domain.UserService;

public interface BaseUserAppServiceComponent {

    UserService userService();

    UserRepository userRepository();

    UserAppService userAppService();

}

package com.eightam.mvp.poc.domain;

import rx.Observable;

public class UserAppService {

    private UserService userService;
    private UserRepository userRepository;

    public UserAppService(UserService userService,
                          UserRepository userRepository) {
        this.userService = userService;
        this.userRepository = userRepository;
    }

    public Observable<User> register(String firstName, String lastName) {
        return Observable.create(subscriber -> {

            User user = userService.register(firstName, lastName);

            if (user != null) {
                userRepository.setUser(user);
                subscriber.onNext(user);
            } else {
                subscriber.onError(new RegistrationException());
            }

            subscriber.onCompleted();

        });
    }

    public Observable<User> getCurrentUser() {
        return Observable.create(subscriber -> {

            User user = userRepository.getUser();

            if (user != null) {
                subscriber.onNext(user);
            } else {
                subscriber.onError(new NoRegisteredUserException());
            }

            subscriber.onCompleted();

        });
    }

}

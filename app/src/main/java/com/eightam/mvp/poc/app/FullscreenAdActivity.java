package com.eightam.mvp.poc.app;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.eightam.mvp.poc.R;
import com.eightam.mvp.poc.coordination.FullscreenAdCoordinator;
import com.eightam.mvp.poc.fragment.FullscreenAdFragment;
import com.eightam.mvp.poc.internal.di.HasComponent;
import com.eightam.mvp.poc.internal.di.component.DaggerFullscreenAdActivityComponent;
import com.eightam.mvp.poc.internal.di.component.FullscreenAdActivityComponent;
import com.eightam.mvp.poc.internal.di.module.FullscreenAdCoordinationModule;
import com.eightam.mvp.poc.internal.di.module.PassOnFullscreenAdNavigationModule;
import com.eightam.mvp.poc.navigation.FullscreenAdNavigator;

import javax.inject.Inject;

public class FullscreenAdActivity
        extends AppCompatActivity
        implements
        FullscreenAdNavigator,
        HasComponent<FullscreenAdActivityComponent> {

    private static final String FRAGMENT_FULLSCREEN_AD = "fullscreenAd";

    protected FullscreenAdActivityComponent fullscreenAdActivityComponent;

    @Inject
    protected FullscreenAdCoordinator fullscreenAdCoordinator;

    public static Intent getStartIntent(Context context) {
        return new Intent(context, FullscreenAdActivity.class);
    }

    @Override
    public void enterFullscreenAdScreen() {
        FullscreenAdFragment fullscreenAdFragment =
                (FullscreenAdFragment) getSupportFragmentManager().findFragmentByTag(FRAGMENT_FULLSCREEN_AD);

        if (fullscreenAdFragment == null) {
            fullscreenAdFragment = FullscreenAdFragment.newInstance();

            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.content, fullscreenAdFragment, FRAGMENT_FULLSCREEN_AD)
                    .commit();
        }
    }

    @Override
    public void enterHomeScreen() {
        Intent intent = MainActivity.getStartIntent(FullscreenAdActivity.this);
        startActivity(intent);
        finish();
    }

    @Override
    public void enterErrorScreen(Throwable t, Bundle args) {
    }

    @Override
    public FullscreenAdActivityComponent getComponent() {
        return fullscreenAdActivityComponent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plain);

        initializeDi();
        fullscreenAdCoordinator.onCreate(savedInstanceState);
    }

    private void initializeDi() {
        fullscreenAdActivityComponent = DaggerFullscreenAdActivityComponent.builder()
                .fullscreenAdCoordinationModule(new FullscreenAdCoordinationModule())
                .passOnFullscreenAdNavigationModule(new PassOnFullscreenAdNavigationModule(FullscreenAdActivity.this))
                .build();

        fullscreenAdActivityComponent.inject(FullscreenAdActivity.this);
    }

}

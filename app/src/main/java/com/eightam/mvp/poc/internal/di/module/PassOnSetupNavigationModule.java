package com.eightam.mvp.poc.internal.di.module;

import com.eightam.mvp.poc.navigation.SetupNavigator;

import dagger.Module;
import dagger.Provides;

@Module
public class PassOnSetupNavigationModule {

    private SetupNavigator setupNavigator;

    public PassOnSetupNavigationModule(SetupNavigator setupNavigator) {
        this.setupNavigator = setupNavigator;
    }

    @Provides
    public SetupNavigator provideNavigator() {
        return setupNavigator;
    }

}

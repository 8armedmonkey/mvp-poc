package com.eightam.mvp.poc.internal.di.component;

import com.eightam.mvp.poc.fragment.RegistrationFragment;
import com.eightam.mvp.poc.fragment.WelcomeFragment;
import com.eightam.mvp.poc.internal.di.component.base.BaseSetupPresentationComponent;
import com.eightam.mvp.poc.internal.di.module.SetupPresentationModule;

import dagger.Component;

@Component(
        dependencies = {
                SetupActivityComponent.class
        },
        modules = {
                SetupPresentationModule.class
        }
)
public interface SetupFragmentComponent
        extends
        SetupActivityComponent,
        BaseSetupPresentationComponent {

    void inject(WelcomeFragment welcomeFragment);

    void inject(RegistrationFragment registrationFragment);

}

package com.eightam.mvp.poc.internal.di.module;

import com.eightam.mvp.poc.presentation.presenter.FullscreenAdPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class FullscreenAdPresentationModule {

    private FullscreenAdPresenter fullscreenAdPresenter;

    @Provides
    public FullscreenAdPresenter provideFullscreenAdPresenter() {
        if (fullscreenAdPresenter == null) {
            fullscreenAdPresenter = new FullscreenAdPresenter();
        }
        return fullscreenAdPresenter;
    }

}

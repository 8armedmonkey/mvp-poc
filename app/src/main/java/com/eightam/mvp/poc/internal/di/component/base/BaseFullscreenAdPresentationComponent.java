package com.eightam.mvp.poc.internal.di.component.base;

import com.eightam.mvp.poc.presentation.presenter.FullscreenAdPresenter;

public interface BaseFullscreenAdPresentationComponent {

    FullscreenAdPresenter fullscreenAdPresenter();

}

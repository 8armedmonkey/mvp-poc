package com.eightam.mvp.poc.presentation.presenter;

import com.eightam.mvp.poc.presentation.view.WelcomeView;

public class WelcomePresenter
        extends BasePresenter<WelcomeView, WelcomePresenter.Delegate>
        implements WelcomeView.Listener {

    @Override
    public void bindView(WelcomeView v) {
        super.bindView(v);
        v.setListener(WelcomePresenter.this);
    }

    @Override
    public void onNavigateAwayFromWelcome() {
        if (delegate != null) {
            delegate.onNavigateAwayFromWelcome();
        }
    }

    public interface Delegate {

        void onNavigateAwayFromWelcome();

    }

}

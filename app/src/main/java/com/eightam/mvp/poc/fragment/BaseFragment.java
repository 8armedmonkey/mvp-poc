package com.eightam.mvp.poc.fragment;

import android.app.Activity;
import android.support.v4.app.Fragment;

import com.eightam.mvp.poc.internal.di.HasComponent;

public class BaseFragment extends Fragment {

    protected <T> T getActivityComponent(Class<T> componentType) {
        Activity activity = getActivity();

        if (activity != null && activity instanceof HasComponent) {
            Object component = ((HasComponent) activity).getComponent();

            if (componentType.isInstance(component)) {
                return componentType.cast(component);
            }
        }
        return null;
    }

}

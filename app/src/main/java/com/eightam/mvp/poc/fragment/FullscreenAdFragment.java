package com.eightam.mvp.poc.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.eightam.mvp.poc.R;
import com.eightam.mvp.poc.coordination.FullscreenAdCoordinator;
import com.eightam.mvp.poc.internal.di.component.DaggerFullscreenAdFragmentComponent;
import com.eightam.mvp.poc.internal.di.component.FullscreenAdActivityComponent;
import com.eightam.mvp.poc.internal.di.module.FullscreenAdPresentationModule;
import com.eightam.mvp.poc.presentation.presenter.FullscreenAdPresenter;
import com.eightam.mvp.poc.presentation.view.FullscreenAdView;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class FullscreenAdFragment extends BaseFragment implements FullscreenAdView {

    @Inject
    protected FullscreenAdCoordinator fullscreenAdCoordinator;

    @Inject
    protected FullscreenAdPresenter presenter;

    private Listener listener;

    public static FullscreenAdFragment newInstance() {
        return new FullscreenAdFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.view_fullscreen_ad, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(FullscreenAdFragment.this, view);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initializeDi();

        presenter.onCreate(savedInstanceState);
        presenter.bindContext(getActivity());
        presenter.bindView(FullscreenAdFragment.this);
        presenter.bindDelegate(fullscreenAdCoordinator);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        if (presenter != null) {
            presenter.onSaveInstanceState(outState);
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if (presenter != null) {
            presenter.onResume();
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        if (presenter != null) {
            presenter.onPause();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if (presenter != null) {
            presenter.onDestroy();
        }
    }

    @Override
    public void setListener(Listener listener) {
        this.listener = listener;
    }

    @OnClick(R.id.button_close)
    protected void closeFullscreenAdFromUi() {
        notifyListenerOnFullscreenAdClosed();
    }

    private void initializeDi() {
        FullscreenAdActivityComponent fullscreenAdActivityComponent =
                getActivityComponent(FullscreenAdActivityComponent.class);

        DaggerFullscreenAdFragmentComponent.builder()
                .fullscreenAdActivityComponent(fullscreenAdActivityComponent)
                .fullscreenAdPresentationModule(new FullscreenAdPresentationModule())
                .build()
                .inject(FullscreenAdFragment.this);
    }

    private void notifyListenerOnFullscreenAdClosed() {
        if (listener != null) {
            listener.onFullscreenAdClosed();
        }
    }

}

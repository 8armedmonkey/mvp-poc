package com.eightam.mvp.poc.presentation.view;

public interface WelcomeView {

    void setListener(Listener listener);

    interface Listener {

        void onNavigateAwayFromWelcome();

    }

}

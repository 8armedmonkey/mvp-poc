package com.eightam.mvp.poc.coordination;

import android.content.Context;
import android.os.Bundle;

public interface Coordinator {

    void onCreate(Bundle savedInstanceState);

    void onDestroy();

    void bindContext(Context context);

    void unbindContext();

}

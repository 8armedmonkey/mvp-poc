package com.eightam.mvp.poc.internal.di.module;

import com.eightam.mvp.poc.coordination.MainCoordinator;
import com.eightam.mvp.poc.navigation.MainNavigator;

import dagger.Module;
import dagger.Provides;

@Module
public class MainCoordinationModule {

    private MainCoordinator mainCoordinator;

    @Provides
    public MainCoordinator provideMainCoordinator(MainNavigator mainNavigator) {
        if (mainCoordinator == null) {
            mainCoordinator = new MainCoordinator(mainNavigator);
        }
        return mainCoordinator;
    }

}

package com.eightam.mvp.poc.internal.di.component;

import com.eightam.mvp.poc.app.MainActivity;
import com.eightam.mvp.poc.internal.di.component.base.BaseMainCoordinationComponent;
import com.eightam.mvp.poc.internal.di.component.base.BaseMainNavigationComponent;
import com.eightam.mvp.poc.internal.di.component.base.BaseUserAppServiceComponent;
import com.eightam.mvp.poc.internal.di.module.MainCoordinationModule;
import com.eightam.mvp.poc.internal.di.module.PassOnMainNavigationModule;
import com.eightam.mvp.poc.internal.di.module.UserAppServiceModule;

import dagger.Component;

@Component(
        modules = {
                UserAppServiceModule.class,
                MainCoordinationModule.class,
                PassOnMainNavigationModule.class
        }
)
public interface MainActivityComponent
        extends
        BaseUserAppServiceComponent,
        BaseMainCoordinationComponent,
        BaseMainNavigationComponent {

    void inject(MainActivity mainActivity);

}

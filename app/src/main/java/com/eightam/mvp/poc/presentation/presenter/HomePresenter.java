package com.eightam.mvp.poc.presentation.presenter;

import com.eightam.mvp.poc.R;
import com.eightam.mvp.poc.domain.User;
import com.eightam.mvp.poc.domain.UserAppService;
import com.eightam.mvp.poc.presentation.mapper.UserMapper;
import com.eightam.mvp.poc.presentation.model.UserPresentation;
import com.eightam.mvp.poc.presentation.view.HomeView;
import com.eightam.mvp.poc.rx.OnErrorIgnore;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class HomePresenter extends BasePresenter<HomeView, Void> {

    private UserAppService userAppService;

    public HomePresenter(UserAppService userAppService) {
        this.userAppService = userAppService;
    }

    public void showMessageToCurrentUser() {
        Subscription s = userAppService.getCurrentUser()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::showMessage, new OnErrorIgnore());

        compositeSubscription.add(s);
    }

    private void showMessage(User user) {
        UserPresentation userPresentation = UserMapper.map(user);
        String message = context.getResources().getString(
                R.string.hello_user,
                userPresentation.getCompleteName());

        if (view != null) {
            view.showUserMessage(message);
        }
    }

}

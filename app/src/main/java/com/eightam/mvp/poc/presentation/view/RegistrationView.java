package com.eightam.mvp.poc.presentation.view;

public interface RegistrationView {

    void setListener(Listener listener);

    interface Listener {

        void onUserDataSubmitted(String firstName, String lastName);

    }

}

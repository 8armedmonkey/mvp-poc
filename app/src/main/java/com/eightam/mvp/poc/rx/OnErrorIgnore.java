package com.eightam.mvp.poc.rx;

import rx.functions.Action1;

public class OnErrorIgnore implements Action1<Throwable> {

    private Throwable throwable;

    @Override
    public void call(Throwable throwable) {
        this.throwable = throwable;
        this.throwable.printStackTrace();
    }

    public Throwable getThrowable() {
        return throwable;
    }

}

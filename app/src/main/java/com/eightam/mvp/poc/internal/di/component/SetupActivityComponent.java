package com.eightam.mvp.poc.internal.di.component;

import com.eightam.mvp.poc.app.SetupActivity;
import com.eightam.mvp.poc.internal.di.component.base.BaseSetupCoordinationComponent;
import com.eightam.mvp.poc.internal.di.component.base.BaseSetupNavigationComponent;
import com.eightam.mvp.poc.internal.di.component.base.BaseUserAppServiceComponent;
import com.eightam.mvp.poc.internal.di.module.PassOnSetupNavigationModule;
import com.eightam.mvp.poc.internal.di.module.SetupCoordinationModule;
import com.eightam.mvp.poc.internal.di.module.UserAppServiceModule;

import dagger.Component;

@Component(
        modules = {
                UserAppServiceModule.class,
                SetupCoordinationModule.class,
                PassOnSetupNavigationModule.class
        }
)
public interface SetupActivityComponent
        extends
        BaseUserAppServiceComponent,
        BaseSetupCoordinationComponent,
        BaseSetupNavigationComponent {

    void inject(SetupActivity setupActivity);

}

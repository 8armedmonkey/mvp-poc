package com.eightam.mvp.poc.app;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import com.eightam.mvp.poc.R;
import com.eightam.mvp.poc.coordination.SetupCoordinator;
import com.eightam.mvp.poc.fragment.RegistrationFragment;
import com.eightam.mvp.poc.fragment.WelcomeFragment;
import com.eightam.mvp.poc.internal.di.HasComponent;
import com.eightam.mvp.poc.internal.di.component.DaggerSetupActivityComponent;
import com.eightam.mvp.poc.internal.di.component.SetupActivityComponent;
import com.eightam.mvp.poc.internal.di.module.PassOnSetupNavigationModule;
import com.eightam.mvp.poc.internal.di.module.SetupCoordinationModule;
import com.eightam.mvp.poc.internal.di.module.UserAppServiceModule;
import com.eightam.mvp.poc.navigation.SetupNavigator;
import com.eightam.mvp.poc.presentation.presenter.HasErrorPresenter;

import javax.inject.Inject;

public class SetupActivity
        extends AppCompatActivity
        implements
        SetupNavigator,
        HasComponent<SetupActivityComponent> {

    private static final String FRAGMENT_WELCOME = "welcome";
    private static final String FRAGMENT_REGISTRATION = "registration";

    private static final String FRAGMENT_TRANSACTION_TO_REGISTRATION = "ft_to_registration";

    protected SetupActivityComponent setupActivityComponent;

    @Inject
    protected SetupCoordinator setupCoordinator;

    @Override
    public void enterWelcomeScreen() {
        WelcomeFragment welcomeFragment =
                (WelcomeFragment) getSupportFragmentManager().findFragmentByTag(FRAGMENT_WELCOME);

        if (welcomeFragment == null) {
            welcomeFragment = WelcomeFragment.newInstance();

            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.content, welcomeFragment, FRAGMENT_WELCOME)
                    .commit();
        }
    }

    @Override
    public void enterRegistrationScreen() {
        RegistrationFragment registrationFragment =
                (RegistrationFragment) getSupportFragmentManager().findFragmentByTag(FRAGMENT_REGISTRATION);

        if (registrationFragment == null) {
            registrationFragment = RegistrationFragment.newInstance();

            getSupportFragmentManager()
                    .beginTransaction()
                    .addToBackStack(FRAGMENT_TRANSACTION_TO_REGISTRATION)
                    .replace(R.id.content, registrationFragment, FRAGMENT_REGISTRATION)
                    .commit();
        }
    }

    @Override
    public void enterFullscreenAdScreen() {
        Intent intent = FullscreenAdActivity.getStartIntent(SetupActivity.this);
        startActivity(intent);
        finish();
    }

    @Override
    public void enterMainScreen() {
        Intent intent = MainActivity.getStartIntent(SetupActivity.this);
        startActivity(intent);
        finish();
    }

    @Override
    public void enterErrorScreen(Throwable t, Bundle args) {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.content);

        if (fragment != null && fragment instanceof HasErrorPresenter) {
            ((HasErrorPresenter) fragment).presentError(t, args);
        }
    }

    @Override
    public SetupActivityComponent getComponent() {
        return setupActivityComponent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plain);

        initializeDi();
        setupCoordinator.onCreate(savedInstanceState);
    }

    private void initializeDi() {
        setupActivityComponent = DaggerSetupActivityComponent.builder()
                .userAppServiceModule(new UserAppServiceModule(SetupActivity.this))
                .setupCoordinationModule(new SetupCoordinationModule())
                .passOnSetupNavigationModule(new PassOnSetupNavigationModule(SetupActivity.this))
                .build();

        setupActivityComponent.inject(SetupActivity.this);
    }

}

package com.eightam.mvp.poc.internal.di.module;

import com.eightam.mvp.poc.coordination.PremiumSetupCoordinator;
import com.eightam.mvp.poc.coordination.SetupCoordinator;
import com.eightam.mvp.poc.domain.UserAppService;
import com.eightam.mvp.poc.navigation.SetupNavigator;

import dagger.Module;
import dagger.Provides;

@Module
public class SetupCoordinationModule {

    @Provides
    public SetupCoordinator provideSetupCoordinator(UserAppService userAppService,
                                                    SetupNavigator setupNavigator) {
        return new PremiumSetupCoordinator(userAppService, setupNavigator);
    }

}
